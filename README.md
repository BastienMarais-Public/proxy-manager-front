# proxy-manager-front

Facilite l'utilisation de l'api [proxy-manager-api](https://gitlab.com/BastienMarais-Public/proxy-manager-api).
## Pré-requis

Pour que l'application fonctionne bien vous devez :
* Connaitre le l'adresse url ou ip vers votre instance de proxy-manager-api
## Installation 

```sh
apt-get update -y
apt-get install -y git
curl -sL https://deb.nodesource.com/setup_14.x | bash -
apt-get install -y nodejs
npm i -g pnpm
git clone https://gitlab.com/BastienMarais-Public/proxy-manager-front.git
cd proxy-manager-front
// FAIRE LA CONFIGURATION DU FICHIER ENV.JS AVANT LA SUITE
pnpm i -P
pnpm start
```

Pour faire fonctionner l'application vous avez besoin compléter le fichier `src/env.js` tel que :
```js
const env = Object.freeze({
    API_URL: "http://localhost:8081"
})

export default env
```

| Clé     | Description                       |
| :------ | :-------------------------------- |
| API_URL | Adresse sur laquelle l'api tourne |
|         |


Une fois que vous avez rempli ces variables il vous suffit de taper :
```sh
pnpm i -P // ou juste npm i --production
pnpm start // ou juste npm start
```

## Fonctionnalités 

Une fois l'application installer vous pouvez :
* Créer et supprimer une entrée dns dans vos domaines 
* Créer un site 
  * Création des fichiers de config nginx
  * Création de l'entrée dns sur ovh
  * Activation du certificat https via certbot si demandé
* Supprimer un site
  * Suppression des fichiers de config nginx
  * Suppression de l'entrée dns sur ovh
  * Désactivation du certificat https via certbot si demandé
